package com.eteration.simplebanking;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.eteration.simplebanking.entity.Account;
import com.eteration.simplebanking.process.DepositTransaction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ModelTest {

    @Test
    public void testCreateAccountAndSetBalance0() {
        Account account = new Account("Özcan Candağ", "111-111");
        assertTrue(account.getOwner().equals("Özcan Candağ"));
        assertTrue(account.getAccountNumber().equals("111-111"));
        assertTrue(account.getBalance() == 0);
    }
}
