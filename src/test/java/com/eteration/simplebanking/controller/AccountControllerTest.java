package com.eteration.simplebanking.controller;

import com.eteration.simplebanking.entity.Account;
import com.eteration.simplebanking.entity.Transaction;
import com.eteration.simplebanking.exception.AccountNotFoundException;
import com.eteration.simplebanking.exception.InsufficientBalanceException;
import com.eteration.simplebanking.process.DepositTransaction;
import com.eteration.simplebanking.process.Mediator;
import com.eteration.simplebanking.process.TransactionProcess;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ContextConfiguration
@AutoConfigureMockMvc
class AccountControllerTest {

    @MockBean
    Mediator mediator;

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;


    @Test
    void createAccount() throws Exception {
        Account request = dummyAccount();
        Account response = dummyAccount();
        response.setId(1111L);

        when(mediator.saveAccount(any(Account.class))).thenReturn(response);
        mockMvc.perform(post("/account/v1")
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.account.id").value(1111L));
    }

    @Test
    void getAccount() throws Exception {
        Account account = dummyAccount();

        when(mediator.findAccount("111-111")).thenReturn(account);
        mockMvc.perform(get("/account/v1/{accountName}", "111-111")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.account.accountNumber").value("111-111"));
    }

    @Test
    void getAccount_Exception() throws Exception {
        when(mediator.findAccount("111-112")).thenThrow(new AccountNotFoundException());
        mockMvc.perform(get("/account/v1/{accountName}", "111-112")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("NOK"))
                .andExpect(jsonPath("$.message").value("Requested Account Not Found."));
    }

    @Test
    void credit() throws Exception {
        when(mediator.processTransaction(any(), any())).thenReturn(dummyTransaction());
        mockMvc.perform(post("/account/v1/credit/{accountNumber}", "111-111")
                .content("{\"amount\":55}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));
    }

    @Test
    void debit() throws Exception {
        when(mediator.processTransaction(any(), any())).thenReturn(dummyTransaction());
        mockMvc.perform(post("/account/v1/debit/{accountNumber}", "111-111")
                .content("{\"amount\":55}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));
    }

    @Test
    void debit_Exception() throws Exception {
        when(mediator.processTransaction(any(), any())).thenThrow(new InsufficientBalanceException());
        mockMvc.perform(post("/account/v1/debit/{accountNumber}", "111-111")
                .content("{\"amount\":55}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("NOK"))
                .andExpect(jsonPath("$.message").value("Account Has No Sufficient Balance For Transaction."));
    }

    private Account dummyAccount() {
        Account account = new Account("Özcan Candağ", "111-111");
        return account;
    }

    private Transaction dummyTransaction() {
        Transaction transaction = new Transaction();
        transaction.setApprovalCode(UUID.randomUUID().toString());
        return transaction;
    }
}