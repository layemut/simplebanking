package com.eteration.simplebanking.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.eteration.simplebanking.process.Mediator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "accounts")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Account {

    @JsonIgnore
    private transient Mediator mediator;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotEmpty
    @Column(name = "owner")
    private String owner;

    @NotEmpty
    @Column(name = "account_number")
    private String accountNumber;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "balance")
    private double balance;

    @JsonManagedReference
    @OneToMany(mappedBy = "account")
    private Set<Transaction> transactions;

    @PrePersist
    public void beforeSave() {
        setCreateDate(new Date());
    }

    public Account(String owner, String accountNumber) {
        this.owner = owner;
        this.accountNumber = accountNumber;
    }

    public void addToBalance(double amount) {
        setBalance(getBalance() + amount);
    }
}
