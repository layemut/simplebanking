package com.eteration.simplebanking.exception;

public class AccountNotFoundException extends Exception {

    private static final long serialVersionUID = -1273069085346581991L;

    public AccountNotFoundException() {
        super("Requested Account Not Found.");
    }

}