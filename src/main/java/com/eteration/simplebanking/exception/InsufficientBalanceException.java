package com.eteration.simplebanking.exception;

public class InsufficientBalanceException extends Exception {

    private static final long serialVersionUID = 9218801962228362540L;

    public InsufficientBalanceException() {
        super("Account Has No Sufficient Balance For Transaction.");
    }

}
