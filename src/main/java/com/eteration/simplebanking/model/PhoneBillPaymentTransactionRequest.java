package com.eteration.simplebanking.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhoneBillPaymentTransactionRequest extends TransactionRequest {
    private String operator;
    private String phoneNumber;
}
