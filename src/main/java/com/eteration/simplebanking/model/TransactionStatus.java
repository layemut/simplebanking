package com.eteration.simplebanking.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TransactionStatus extends BaseResponse {
    private String approvalCode;

    public static TransactionStatus successResponse(String approvalCode) {
        TransactionStatus response = new TransactionStatus();
        response.setStatus("OK");
        response.setApprovalCode(approvalCode);
        return response;
    }
}
