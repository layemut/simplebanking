package com.eteration.simplebanking.model;

import com.eteration.simplebanking.entity.Account;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AccountResponse extends BaseResponse {
    Account account;

    public static AccountResponse successResponse(Account account) {
        AccountResponse response = new AccountResponse();
        response.setStatus("OK");
        response.setMessage("Success");
        response.setAccount(account);
        return response;
    }
}