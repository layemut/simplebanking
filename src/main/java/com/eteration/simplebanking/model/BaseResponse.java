package com.eteration.simplebanking.model;

import lombok.Data;

@Data
public class BaseResponse {
    String status;
    String message;
}