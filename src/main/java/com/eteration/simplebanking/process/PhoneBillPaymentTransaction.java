package com.eteration.simplebanking.process;

import com.eteration.simplebanking.entity.Account;
import com.eteration.simplebanking.entity.Transaction;
import com.eteration.simplebanking.exception.InsufficientBalanceException;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneBillPaymentTransaction extends BaseProcess {

    private String operator;
    private String phoneNumber;

    public PhoneBillPaymentTransaction(String operator, String phoneNumber, double amount) {
        this.operator = operator;
        this.phoneNumber = phoneNumber;
        transaction = new Transaction();
        transaction.setAmount(-amount);
        transaction.setType(getClass().getSimpleName());
        transaction.setNotes(String.format("Phone bill payment transaction operator/phone-number %s/%s", operator, phoneNumber));
    }

    @Override
    public Transaction process(Mediator mediator, Account account) throws InsufficientBalanceException {
        if (account.getBalance() < Math.abs(transaction.getAmount())) {
            throw new InsufficientBalanceException();
        }
        transaction.setAccount(account);
        account.addToBalance(transaction.getAmount());

        mediator.saveAccount(account);
        return mediator.saveTransaction(transaction);
    }
}
