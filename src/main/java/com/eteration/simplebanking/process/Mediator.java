package com.eteration.simplebanking.process;

import com.eteration.simplebanking.entity.Account;
import com.eteration.simplebanking.entity.Transaction;
import com.eteration.simplebanking.exception.AccountNotFoundException;
import com.eteration.simplebanking.exception.InsufficientBalanceException;
import com.eteration.simplebanking.repository.AccountRepository;
import com.eteration.simplebanking.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class Mediator {

    AccountRepository accountRepository;
    TransactionRepository transactionRepository;

    @Autowired
    public Mediator(AccountRepository accountRepository, TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    public Transaction processTransaction(String accountNumber, TransactionProcess transactionProcess)
            throws AccountNotFoundException, InsufficientBalanceException {
        log.info("Processing Transaction: " + transactionProcess);
        Account found = findAccount(accountNumber);
        return transactionProcess.process(this, found);
    }

    public Account saveAccount(Account account) {
        return accountRepository.save(account);
    }

    public Transaction saveTransaction(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    public Account findAccount(String accountNumber) throws AccountNotFoundException {
        return accountRepository.findByAccountNumber(accountNumber).orElseThrow(AccountNotFoundException::new);
    }
}