package com.eteration.simplebanking.process;

import com.eteration.simplebanking.entity.Account;
import com.eteration.simplebanking.entity.Transaction;
import com.eteration.simplebanking.exception.InsufficientBalanceException;

public interface TransactionProcess {
    Transaction process(Mediator mediator, Account account) throws InsufficientBalanceException;
}