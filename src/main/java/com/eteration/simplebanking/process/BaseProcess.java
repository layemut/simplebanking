package com.eteration.simplebanking.process;

import com.eteration.simplebanking.entity.Transaction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BaseProcess implements TransactionProcess {
    Transaction transaction;
}