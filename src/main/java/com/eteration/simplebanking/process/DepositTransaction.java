package com.eteration.simplebanking.process;

import com.eteration.simplebanking.entity.Account;
import com.eteration.simplebanking.entity.Transaction;
import com.eteration.simplebanking.exception.InsufficientBalanceException;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepositTransaction extends BaseProcess {

    public DepositTransaction(double amount) {
        transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setType(getClass().getSimpleName());
        transaction.setNotes("Basic deposit transaction");
    }

    @Override
    public Transaction process(Mediator mediator, Account account) throws InsufficientBalanceException {
        transaction.setAccount(account);

        account.addToBalance(transaction.getAmount());

        mediator.saveAccount(account);
        return mediator.saveTransaction(transaction);
    }

}
