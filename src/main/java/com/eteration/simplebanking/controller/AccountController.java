package com.eteration.simplebanking.controller;

import javax.validation.Valid;

import com.eteration.simplebanking.entity.Account;
import com.eteration.simplebanking.entity.Transaction;
import com.eteration.simplebanking.exception.AccountNotFoundException;
import com.eteration.simplebanking.exception.InsufficientBalanceException;
import com.eteration.simplebanking.model.*;
import com.eteration.simplebanking.process.DepositTransaction;
import com.eteration.simplebanking.process.Mediator;
import com.eteration.simplebanking.process.PhoneBillPaymentTransaction;
import com.eteration.simplebanking.process.WithdrawalTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/account/v1")
@Slf4j
public class AccountController {

    Mediator mediator;

    @Autowired
    public AccountController(Mediator mediator) {
        this.mediator = mediator;
    }

    @PostMapping
    public ResponseEntity<AccountResponse> createAccount(@RequestBody @Valid Account account) throws Exception {
        return ResponseEntity.ok().body(AccountResponse.successResponse(mediator.saveAccount(account)));
    }

    @GetMapping("/{accountNumber}")
    public ResponseEntity<AccountResponse> getAccount(@PathVariable("accountNumber") String accountNumber)
            throws Exception {
        return ResponseEntity.ok().body(AccountResponse.successResponse(mediator.findAccount(accountNumber)));
    }

    @PostMapping("/credit/{accountNumber}")
    public ResponseEntity<TransactionStatus> credit(@PathVariable("accountNumber") String accountNumber,
                                                    @RequestBody TransactionRequest request) throws Exception {
        log.info("Credit request for account number: {}", accountNumber);

        Transaction saved = mediator.processTransaction(accountNumber, new DepositTransaction(request.getAmount()));

        return ResponseEntity.ok().body(TransactionStatus.successResponse(saved.getApprovalCode()));
    }

    @PostMapping("/debit/{accountNumber}")
    public ResponseEntity<TransactionStatus> debit(@PathVariable("accountNumber") String accountNumber,
                                                   @RequestBody TransactionRequest request) throws Exception {
        log.info("Debit request for account number: {}", accountNumber);

        Transaction saved = mediator.processTransaction(accountNumber, new WithdrawalTransaction(request.getAmount()));

        return ResponseEntity.ok().body(TransactionStatus.successResponse(saved.getApprovalCode()));
    }

    @PostMapping("/bill/payment/phone/{accountNumber}")
    public ResponseEntity<TransactionStatus> phoneBillPayment(@PathVariable("accountNumber") String accountNumber,
                                                              @RequestBody PhoneBillPaymentTransactionRequest request) throws Exception {
        log.info("Debit request for account number: {}", accountNumber);

        Transaction saved = mediator.processTransaction(accountNumber, new PhoneBillPaymentTransaction(request.getOperator(), request.getPhoneNumber(), request.getAmount()));

        return ResponseEntity.ok().body(TransactionStatus.successResponse(saved.getApprovalCode()));
    }


    @ExceptionHandler({AccountNotFoundException.class, InsufficientBalanceException.class})
    public ResponseEntity<BaseResponse> handleException(Exception exception) {
        BaseResponse response = new BaseResponse();
        response.setStatus("NOK");
        response.setMessage(exception.getMessage());
        return ResponseEntity.badRequest().body(response);
    }

}